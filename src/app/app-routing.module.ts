import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './components/phones/detail';
import { ListComponent } from './components/phones/list';

const routes: Routes = [
  { path: 'detail', component: DetailComponent },
  { path: 'list', component: ListComponent },
  { path: '', component: ListComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
